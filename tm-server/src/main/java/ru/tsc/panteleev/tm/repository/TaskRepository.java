package ru.tsc.panteleev.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.repository.ITaskRepository;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    private static final String TABLE_NAME = "\"TM_TASK\"";

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Task fetch(@NotNull final ResultSet rowSet) {
        @NotNull final Task task = new Task();
        task.setId(rowSet.getString("row_id"));
        task.setName(rowSet.getString("name"));
        task.setDescription(rowSet.getString("description"));
        task.setStatus(Status.toStatus(rowSet.getString("status")));
        task.setUserId(rowSet.getString("user_id"));
        task.setProjectId(rowSet.getString("project_id"));
        task.setCreated(rowSet.getTimestamp("created_dt"));
        task.setDateBegin(rowSet.getTimestamp("begin_dt"));
        task.setDateEnd(rowSet.getTimestamp("end_dt"));
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task add(@Nullable final Task task) {
        if (task == null) return null;
        @NotNull final String sql = String.format(
                "INSERT INTO %s (row_id, name, description, status, user_id, project_id, created_dt, begin_dt, end_dt) "
                        + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getId());
            statement.setString(2, task.getName());
            statement.setString(3, task.getDescription());
            statement.setString(4, task.getStatus().toString());
            statement.setString(5, task.getUserId());
            statement.setString(6, task.getProjectId());
            statement.setTimestamp(7, convertDateToTimestamp(task.getCreated()));
            statement.setTimestamp(8, convertDateToTimestamp(task.getDateBegin()));
            statement.setTimestamp(9, convertDateToTimestamp(task.getDateEnd()));
            statement.executeUpdate();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task update(@NotNull final Task task) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET name = ?, description = ?, status = ?, project_id = ? WHERE row_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getName());
            statement.setString(2, task.getDescription());
            statement.setString(3, task.getStatus().toString());
            statement.setString(4, task.getProjectId());
            statement.setString(5, task.getId());
            statement.executeUpdate();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (userId == null) return Collections.emptyList();
        @NotNull final List<Task> tasks = new Vector<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE user_id = ? AND project_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, projectId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            while (rowSet.next())
                tasks.add(fetch(rowSet));
            return tasks;
        }
    }

    @Override
    @SneakyThrows
    public void removeTasksByProjectId(@NotNull final String projectId) {
        @NotNull final String query = String.format("DELETE FROM %s WHERE project_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, projectId);
            statement.executeUpdate();
        }
    }

    @NotNull
    @Override
    public Task create(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        return add(task);
    }

    @NotNull
    @Override
    public Task create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        @NotNull final Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        return add(task);
    }

}
